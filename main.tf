locals {
  enabled = var.alb_module_enabled ? true : false
  tags = merge(
    var.alb_tags,
    {
      terraform = true
    }
  )
}


# ---------------------------
# Network/Security resources
# ---------------------------
resource "aws_security_group" "alb" {
  # Provides a security group resource.
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = format("%s-alb-sg-", var.alb_name)
  # (Optional, Forces new resource) The security group description. Defaults to "Managed by Terraform".
  description = "The Security Group to control access to the [${var.alb_name}] ALB."
  # (Optional, Forces new resource) The VPC ID.
  vpc_id = var.alb_vpc_id
  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      Name = format("%s-alb-sg", var.alb_name)
    }
  )
}
resource "aws_security_group_rule" "egress_all" {
  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = local.enabled ? 1 : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "egress"
  # (Optional) Description of the rule.
  description = "Allow all Egress traffic from the ALB."
  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = 0
  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = 0
  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "-1"
  # (Optional) List of CIDR blocks. Cannot be specified with `source_security_group_id`.
  cidr_blocks = ["0.0.0.0/0"]
  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.alb.*.id)
}
resource "aws_security_group_rule" "ingress_http" {
  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = (local.enabled && var.alb_http_listener_enabled) ? 1 : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "ingress"
  # (Optional) Description of the rule.
  description = "Allow inbound HTTP requests."
  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = var.alb_http_listener_port
  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = var.alb_http_listener_port
  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "tcp"
  # (Optional) List of CIDR blocks. Cannot be specified with `source_security_group_id`.
  cidr_blocks = var.alb_http_ingress_cidr_blocks
  # (Optional) List of prefix list IDs (for allowing access to VPC endpoints).
  prefix_list_ids = var.alb_http_ingress_prefix_list_ids
  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.alb.*.id)
}
resource "aws_security_group_rule" "ingress_https" {
  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = (local.enabled && var.alb_https_listener_enabled) ? 1 : 0
  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "ingress"
  # (Optional) Description of the rule.
  description = "Allow inbound HTTPS requests."
  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = var.alb_https_listener_port
  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = var.alb_https_listener_port
  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "tcp"
  # (Optional) List of CIDR blocks. Cannot be specified with `source_security_group_id`.
  cidr_blocks = var.alb_https_ingress_cidr_blocks
  # (Optional) List of prefix list IDs (for allowing access to VPC endpoints).
  prefix_list_ids = var.alb_https_ingress_prefix_list_ids
  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.alb.*.id)
}


# ---------------
# S3 Logs bucket
# ---------------
module "access_logs" {
  source = "git::https://gitlab.com/codeassist/terraform/modules/aws-s3-lb-logs.git?ref=tags/1.0.4"

  # --------------------------
  # General/Common parameters
  # --------------------------
  # Whether to create the resources ("false" prevents the module from creating any resources).
  s3_lb_logs_module_enabled = local.enabled && var.alb_access_logs_enabled

  # ---------------------
  # S3 bucket parameters
  # ---------------------
  # (Required) Creates a unique bucket name beginning with the specified prefix.
  s3_lb_logs_bucket_prefix = format("%s-access-logs-", var.alb_name)
  # A mapping of tags to assign to the bucket.
  s3_lb_logs_bucket_tags = local.tags
  # A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed
  # without error. These objects are not recoverable.
  s3_lb_logs_bucket_force_destroy = var.alb_access_logs_force_destroy_s3_bucket
  # A state of versioning. Versioning is a means of keeping multiple variants of an object in the same bucket.
  s3_lb_logs_bucket_versioning_enabled = false
  # The server-side encryption algorithm to use.
  s3_lb_logs_bucket_sse_algorithm = "AES256"
  # The AWS KMS master key ARN used for the SSE-KMS encryption.
  s3_lb_logs_bucket_kms_master_key_arn = ""
  # Enable lifecycle events on this bucket.
  s3_lb_logs_bucket_lifecycle_rule_enabled = true
  # Specifies when noncurrent object versions expire.
  s3_lb_logs_bucket_noncurrent_version_expiration_days = 90
  # Specifies when noncurrent object versions transitions.
  s3_lb_logs_bucket_noncurrent_version_transition_days = 30
  # Number of days to persist in the standard storage tier before moving to the infrequent access tier.
  s3_lb_logs_bucket_standard_transition_days = 30
  # Number of days after which to move the data to the glacier storage tier.
  s3_lb_logs_bucket_glacier_transition_days = 60
  # Number of days after which to expunge the objects.
  s3_lb_logs_bucket_expiration_days = 90
}


# --------------------------
# Application Load Balancer
# --------------------------
resource "aws_lb" "alb" {
  # Provides a Load Balancer resource.
  # Note(!): `aws_alb` is known as `aws_lb`. The functionality is identical.
  count = local.enabled ? 1 : 0

  # (Optional) The name of the LB. This name must be unique within your AWS account, can have a maximum of 32
  # characters, must contain only alphanumeric characters or hyphens, and must not begin or end with a hyphen.
  # If not specified, Terraform will autogenerate a name beginning with tf-lb.
  name = var.alb_name
  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      Name = var.alb_name
    }
  )

  # (Optional) If true, the LB will be internal.
  internal = var.alb_internal
  # (Optional) The type of load balancer to create. Possible values are:
  #   * application
  #   * network
  load_balancer_type = "application"
  # (Optional) A list of security group IDs to assign to the LB. Only valid for Load Balancers of type `application`.
  security_groups = compact(
    concat(
      var.alb_security_group_ids,
      [
        join("", aws_security_group.alb.*.id)
      ]
    ),
  )
  # (Optional) A list of subnet IDs to attach to the LB. Subnets cannot be updated for Load Balancers
  # of type `network`. Changing this value for load balancers of type `network` will force a recreation of the
  # resource.
  subnets = var.alb_subnet_ids
  # (Optional) The time in seconds that the connection is allowed to be idle. Only valid for Load Balancers
  # of type `application`. Default: 60.
  idle_timeout = var.alb_idle_timeout
  # (Optional) If true, deletion of the load balancer will be disabled via the AWS API. This will prevent Terraform
  # from deleting the load balancer. Defaults to `false`.
  enable_deletion_protection = var.alb_deletion_protection_enabled
  # (Optional) If true, cross-zone load balancing of the load balancer will be enabled. This is a `network` load
  # balancer feature. Defaults to `false`.
  enable_cross_zone_load_balancing = var.alb_cross_zone_load_balancing_enabled
  # (Optional) Indicates whether HTTP/2 is enabled in `application` load balancers. Defaults to `true`.
  enable_http2 = var.alb_http2_enabled
  # (Optional) The type of IP addresses used by the subnets for your load balancer. The possible values are:
  #   * ipv4
  #   * dualstack
  ip_address_type = var.alb_ip_address_type

  # (Optional) An Access Logs block.
  access_logs {
    # (Optional) Boolean to enable / disable `access_logs`. Defaults to `false`, even when `bucket` is specified.
    enabled = var.alb_access_logs_enabled
    # (Required) The S3 bucket name to store the logs in.
    bucket = module.access_logs.s3_lb_logs_bucket_id
  }
}


# -------------------------
# Default ALB Target Group
# -------------------------
resource "aws_lb_target_group" "default" {
  # Provides a Target Group resource for use with Load Balancer resources.
  # Note: `aws_alb_target_group` is known as `aws_lb_target_group`. The functionality is identical.
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) The name of the target group. If omitted, Terraform will assign a random, unique name.
  name = format("%s-default-tg", var.alb_name)
  # (Optional, Forces new resource) The port on which targets receive traffic, unless overridden when registering
  # a specific target. Required when `target_type` is `instance` or `ip`. Does not apply when `target_type` is `lambda`.
  port = var.alb_default_target_group_port
  # (Optional, Forces new resource) The protocol to use for routing traffic to the targets. Should be one of:
  #   * TCP
  #   * TLS
  #   * UDP
  #   * TCP_UDP
  #   * HTTP
  #   * HTTPS
  # Required when `target_type` is `instance` or `ip`. Does not apply when `target_type` is `lambda`.
  protocol = "HTTP"
  # (Optional, Forces new resource) The identifier of the VPC in which to create the target group. Required when
  # `target_type` is `instance` or `ip`. Does not apply when `target_type` is `lambda`.
  vpc_id = var.alb_vpc_id
  # (Optional) The amount time for Elastic Load Balancing to wait before changing the state of a deregistering target
  # from draining to unused. The range is 0-3600 seconds. The default value is 300 seconds.
  deregistration_delay = var.alb_default_target_group_deregistration_delay
  # (Optional, Forces new resource) The type of target that you must specify when registering targets with this target
  # group. The possible values are:
  #   * instance (targets are specified by instance ID)
  #   * ip (targets are specified by IP address)
  #   * lambda (targets are specified by lambda arn).
  # The default is `instance`. Note that you can't specify targets for a target group using both instance IDs and IP
  # addresses. If the target type is ip, specify IP addresses from the subnets of the virtual private cloud (VPC) for
  # the target group, the RFC 1918 range (10.0.0.0/8, 172.16.0.0/12, and 192.168.0.0/16), and the RFC 6598
  # range (100.64.0.0/10). You can't specify publicly routable IP addresses.
  target_type = var.alb_default_target_group_target_type

  # (Optional) A Health Check block.
  health_check {
    # (Optional) Indicates whether health checks are enabled.
    enabled = var.alb_default_target_group_health_check_enabled
    # (Optional) The approximate amount of time, in seconds, between health checks of an individual target.
    # Minimum value 5 seconds, Maximum value 300 seconds. For `lambda` target groups, it needs to be greater as the
    # `timeout` of the underlying `lambda`.
    interval = var.alb_default_target_group_health_check_interval
    # (Required for HTTP/HTTPS ALB) The destination for the health check request. Applies to Application Load Balancers
    # only (HTTP/HTTPS), not Network Load Balancers (TCP).
    path = var.alb_default_target_group_health_check_path
    # (Optional) The amount of time, in seconds, during which no response means a failed health check. For Application
    # Load Balancers, the range is 2 to 120 seconds, and the default is 5 seconds for the `instance` target type and
    # 30 seconds for the `lambda` target type. For Network Load Balancers, you cannot set a custom value, and the
    # default is 10 seconds for TCP and HTTPS health checks and 6 seconds for HTTP health checks.
    timeout = var.alb_default_target_group_health_check_timeout
    # (Optional) The number of consecutive health checks successes required before considering an unhealthy target
    # healthy. Defaults to 3.
    healthy_threshold = var.alb_default_target_group_health_check_healthy_threshold
    # (Optional) The number of consecutive health check failures required before considering the target unhealthy.
    # For Network Load Balancers, this value must be the same as the healthy_threshold. Defaults to 3.
    unhealthy_threshold = var.alb_default_target_group_health_check_unhealthy_threshold
    # (Required for HTTP/HTTPS ALB) The HTTP codes to use when checking for a successful response from a target. You
    # can specify multiple values (for example, "200,202") or a range of values (for example, "200-299"). Applies to
    # Application Load Balancers only (HTTP/HTTPS), not Network Load Balancers (TCP).
    matcher = var.alb_default_target_group_health_check_matcher
  }

  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      Name = format("%s-default-tg", var.alb_name)
    }
  )
}


resource "aws_lb_listener" "http" {
  # Provides a Load Balancer Listener resource.
  # Note(!): `aws_alb_listener` is known as `aws_lb_listener`. The functionality is identical.
  count = (local.enabled && var.alb_http_listener_enabled) ? 1 : 0

  # (Required, Forces New Resource) The ARN of the load balancer.
  load_balancer_arn = join("", aws_lb.alb.*.arn)
  # (Required) The port on which the load balancer is listening.
  port = var.alb_http_listener_port
  # (Optional) The protocol for connections from clients to the load balancer. Valid values are:
  #   * TCP
  #   * TLS
  #   * UDP
  #   * TCP_UDP
  #   * HTTP
  #   * HTTPS
  # NOTE: Please note that listeners that are attached to Application Load Balancers must use either `HTTP` or `HTTPS`
  # protocols while listeners that are attached to Network Load Balancers must use the `TCP` protocol.
  protocol = "HTTP"

  # (Required) An Action block.
  # See https://www.terraform.io/docs/providers/aws/r/lb_listener.html to find more available configurations.
  default_action {
    # (Required) The type of routing action. Valid values are:
    #   * forward
    #   * redirect
    #   * fixed-response
    #   * (not implemented) authenticate-cognito
    #   * (not implemented) authenticate-oidc.
    type = var.alb_http_listener_default_action_type
    # (Optional) The ARN of the Target Group to which to route traffic. Required if type is `forward`.
    target_group_arn = length(var.alb_http_listener_forward_target_group) > 0 ? var.alb_http_listener_forward_target_group : join("", aws_lb_target_group.default.*.arn)

    # (Optional) Information for creating a redirect action. Required if type is `redirect`.
    dynamic "redirect" {
      for_each = lower(var.alb_http_listener_default_action_type) == "redirect" ? ["true"] : []

      # NOTE: You can reuse URI components using the following reserved keywords:
      #   * #{protocol}
      #   * #{host}
      #   * #{port}
      #   * #{path} (the leading "/" is removed)
      #   * #{query}
      content {
        # (Optional) The hostname. This component is not percent-encoded. The hostname can contain #{host}.
        host = var.alb_http_listener_redirect_host
        # (Optional) The absolute path, starting with the leading "/". This component is not percent-encoded.
        # The `path` can contain #{host}, #{path}, and #{port}.
        path = var.alb_http_listener_redirect_path
        # (Optional) The port. Specify a value from 1 to 65535 or #{port}.
        port = var.alb_http_listener_redirect_port
        # (Optional) The protocol. Valid values are HTTP, HTTPS, or #{protocol}.
        protocol = var.alb_http_listener_redirect_protocol
        # (Optional) The query parameters, URL-encoded when necessary, but not percent-encoded. Do not include the
        # leading "?".
        query = var.alb_http_listener_redirect_query
        # (Required) The HTTP redirect code. The redirect is either permanent (HTTP_301) or temporary (HTTP_302).
        status_code = var.alb_http_listener_redirect_status_code
      }
    }

    # (Optional) Information for creating an action that returns a custom HTTP response. Required if `type` is
    # `fixed-response`.
    dynamic "fixed_response" {
      for_each = lower(var.alb_http_listener_default_action_type) == "fixed-response" ? ["true"] : []

      content {
        # (Required) The content type. Valid values are:
        #   * text/plain
        #   * text/css
        #   * text/html
        #   * application/javascript
        #   * application/json
        content_type = var.alb_http_listener_fixed_response_content_type
        # (Optional) The message body.
        message_body = var.alb_http_listener_fixed_response_message_body
        # (Optional) The HTTP response code. Valid values are 2XX, 4XX, or 5XX.
        status_code = var.alb_http_listener_fixed_response_status_code
      }
    }
  }
}

resource "aws_lb_listener" "https" {
  # Provides a Load Balancer Listener resource.
  # Note(!): `aws_alb_listener` is known as `aws_lb_listener`. The functionality is identical.
  count = (local.enabled && var.alb_https_listener_enabled) ? 1 : 0

  # (Required, Forces New Resource) The ARN of the load balancer.
  load_balancer_arn = join("", aws_lb.alb.*.arn)
  # (Required) The port on which the load balancer is listening.
  port = var.alb_https_listener_port
  # (Optional) The protocol for connections from clients to the load balancer. Valid values are:
  #   * TCP
  #   * TLS
  #   * UDP
  #   * TCP_UDP
  #   * HTTP
  #   * HTTPS
  # NOTE: Please note that listeners that are attached to Application Load Balancers must use either `HTTP` or `HTTPS`
  # protocols while listeners that are attached to Network Load Balancers must use the `TCP` protocol.
  protocol = "HTTPS"
  # (Optional) The name of the SSL Policy for the listener. Required if protocol is `HTTPS` or `TLS`.
  ssl_policy = var.alb_https_listener_ssl_policy
  # (Optional) The ARN of the default SSL server certificate. Exactly one certificate is required if the protocol is
  # `HTTPS`. For adding additional SSL certificates, see the `aws_lb_listener_certificate` resource.
  certificate_arn = var.alb_https_listener_certificate_arn

  # (Required) An Action block.
  # See https://www.terraform.io/docs/providers/aws/r/lb_listener.html to find more available configurations.
  default_action {
    # (Required) The type of routing action. Valid values are:
    #   * forward
    #   * redirect
    #   * fixed-response
    #   * authenticate-cognito
    #   * authenticate-oidc.
    type = var.alb_https_listener_default_action_type
    # (Optional) The ARN of the Target Group to which to route traffic. Required if type is `forward`.
    target_group_arn = length(var.alb_https_listener_forward_target_group) > 0 ? var.alb_https_listener_forward_target_group : join("", aws_lb_target_group.default.*.arn)

    # (Optional) Information for creating a redirect action. Required if type is `redirect`.
    dynamic "redirect" {
      for_each = lower(var.alb_https_listener_default_action_type) == "redirect" ? ["true"] : []

      # NOTE: You can reuse URI components using the following reserved keywords:
      #   * #{protocol}
      #   * #{host}
      #   * #{port}
      #   * #{path} (the leading "/" is removed)
      #   * #{query}
      content {
        # (Optional) The hostname. This component is not percent-encoded. The hostname can contain #{host}.
        host = var.alb_https_listener_redirect_host
        # (Optional) The absolute path, starting with the leading "/". This component is not percent-encoded.
        # The `path` can contain #{host}, #{path}, and #{port}.
        path = var.alb_https_listener_redirect_path
        # (Optional) The port. Specify a value from 1 to 65535 or #{port}.
        port = var.alb_https_listener_redirect_port
        # (Optional) The protocol. Valid values are HTTP, HTTPS, or #{protocol}.
        protocol = var.alb_https_listener_redirect_protocol
        # (Optional) The query parameters, URL-encoded when necessary, but not percent-encoded. Do not include the
        # leading "?".
        query = var.alb_https_listener_redirect_query
        # (Required) The HTTP redirect code. The redirect is either permanent (HTTP_301) or temporary (HTTP_302).
        status_code = var.alb_https_listener_redirect_status_code
      }
    }

    # (Optional) Information for creating an action that returns a custom HTTP response. Required if `type` is
    # `fixed-response`.
    dynamic "fixed_response" {
      for_each = lower(var.alb_https_listener_default_action_type) == "fixed-response" ? ["true"] : []

      content {
        # (Required) The content type. Valid values are:
        #   * text/plain
        #   * text/css
        #   * text/html
        #   * application/javascript
        #   * application/json
        content_type = var.alb_https_listener_fixed_response_content_type
        # (Optional) The message body.
        message_body = var.alb_https_listener_fixed_response_message_body
        # (Optional) The HTTP response code. Valid values are 2XX, 4XX, or 5XX.
        status_code = var.alb_https_listener_fixed_response_status_code
      }
    }
  }
}
