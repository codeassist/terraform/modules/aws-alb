provider "aws" {
  region = var.region
}

module "vpc" {
  source     = "git::https://github.com/cloudposse/terraform-aws-vpc.git?ref=tags/0.8.1"
  namespace  = var.namespace
  stage      = var.stage
  name       = var.name
  delimiter  = var.delimiter
  attributes = var.attributes
  cidr_block = var.vpc_cidr_block
  tags       = var.tags
}

module "subnets" {
  source               = "git::https://github.com/cloudposse/terraform-aws-dynamic-subnets.git?ref=tags/0.16.1"
  availability_zones   = var.availability_zones
  namespace            = var.namespace
  stage                = var.stage
  name                 = var.name
  attributes           = var.attributes
  delimiter            = var.delimiter
  vpc_id               = module.vpc.vpc_id
  igw_id               = module.vpc.igw_id
  cidr_block           = module.vpc.vpc_cidr_block
  nat_gateway_enabled  = false
  nat_instance_enabled = false
  tags                 = var.tags
}

module "alb" {
  source                                  = "../.."
  namespace                               = var.namespace
  stage                                   = var.stage
  name                                    = var.name
  attributes                              = var.attributes
  delimiter                               = var.delimiter
  alb_vpc_id                                  = module.vpc.vpc_id
  alb_security_group_ids                      = [module.vpc.vpc_default_security_group_id]
  alb_subnet_ids                              = module.subnets.public_subnet_ids
  alb_internal                                = var.internal
  alb_http_listener_enabled                            = var.http_enabled
  alb_access_logs_enabled                     = var.access_logs_enabled
  alb_access_logs_force_destroy_s3_bucket = var.alb_access_logs_s3_bucket_force_destroy
  access_logs_region                      = var.access_logs_region
  alb_cross_zone_load_balancing_enabled       = var.cross_zone_load_balancing_enabled
  alb_http2_enabled                           = var.http2_enabled
  alb_idle_timeout                            = var.idle_timeout
  alb_ip_address_type                         = var.ip_address_type
  alb_deletion_protection_enabled             = var.deletion_protection_enabled
  alb_default_target_group_deregistration_delay                    = var.deregistration_delay
  alb_default_target_group_health_check_path                       = var.health_check_path
  alb_default_target_group_health_check_timeout                    = var.health_check_timeout
  alb_default_target_group_health_check_healthy_threshold          = var.health_check_healthy_threshold
  alb_default_target_group_health_check_unhealthy_threshold        = var.health_check_unhealthy_threshold
  alb_default_target_group_health_check_interval                   = var.health_check_interval
  alb_default_target_group_health_check_matcher                    = var.health_check_matcher
  alb_default_target_group_port                       = var.target_group_port
  alb_default_target_group_target_type                = var.target_group_target_type
  tags                                    = var.tags
}
