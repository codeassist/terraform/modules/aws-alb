# --------------------------
# General/Common parameters
# --------------------------
variable "alb_module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # "false" prevents the module from creating any resources
  default = false
}

variable "alb_name" {
  description = "(Required) The name of the LB."
  # This name:
  #   - must be unique within your AWS account,
  #   - can have a maximum of 32 characters,
  #   - must contain only alphanumeric characters or hyphens,
  #   - and must not begin or end with a hyphen.
  # If not specified, Terraform will autogenerate a name beginning with `tf-lb`.
  type = string
}
variable "alb_tags" {
  description = "A mapping of tags to assign to the resource."
  type        = map(string)
  default     = {}
}


# ----------------------------
# Network/Security parameters
# ----------------------------
variable "alb_vpc_id" {
  description = "(Required) VPC ID to associate with ALB."
  type        = string
}

variable "alb_http_listener_enabled" {
  description = "A boolean flag to enable/disable HTTP listener."
  type        = bool
  default     = true
}
variable "alb_http_listener_port" {
  description = "The port for the HTTP listener."
  type        = number
  default     = 80
}
variable "alb_http_ingress_cidr_blocks" {
  description = "List of CIDR blocks to allow in HTTP security group."
  type        = list(string)
  default     = ["0.0.0.0/0"]
}
variable "alb_http_ingress_prefix_list_ids" {
  description = "List of prefix list IDs for allowing access to HTTP ingress security group."
  type        = list(string)
  default     = []
}

variable "alb_https_listener_enabled" {
  description = "A boolean flag to enable/disable HTTPS listener."
  type        = bool
  default     = false
}
variable "alb_https_listener_port" {
  description = "The port for the HTTPS listener."
  type        = number
  default     = 443
}
variable "alb_https_ingress_cidr_blocks" {
  description = "List of CIDR blocks to allow in HTTPS security group."
  type        = list(string)
  default     = ["0.0.0.0/0"]
}
variable "alb_https_ingress_prefix_list_ids" {
  description = "List of prefix list IDs for allowing access to HTTPS ingress security group."
  type        = list(string)
  default     = []
}


# --------------------------
# S3 Logs bucket parameters
# --------------------------
variable "alb_access_logs_enabled" {
  description = "A boolean flag to enable/disable access_logs."
  type        = bool
  default     = false
}
variable "alb_access_logs_force_destroy_s3_bucket" {
  description = "A boolean that indicates all objects should be deleted from the ALB access logs S3 bucket so that the bucket can be destroyed without error."
  type        = bool
  default     = false
}


# ---------------
# ALB parameters
# ---------------
variable "alb_internal" {
  description = "A boolean flag to determine whether the ALB should be internal."
  type        = bool
  default     = false
}
variable "alb_security_group_ids" {
  description = "A list of additional security group IDs to assign to the ALB."
  type        = list(string)
  default     = []
}
variable "alb_subnet_ids" {
  description = "(Required) A list of subnet IDs to associate with ALB."
  type        = list(string)
}
variable "alb_idle_timeout" {
  description = "The time in seconds that the connection is allowed to be idle."
  type        = number
  default     = 60
}
variable "alb_deletion_protection_enabled" {
  description = "A boolean flag to enable/disable deletion protection for ALB."
  type        = bool
  default     = false
}
variable "alb_cross_zone_load_balancing_enabled" {
  description = "A boolean flag to enable/disable cross zone load balancing."
  type        = bool
  default     = true
}
variable "alb_http2_enabled" {
  description = "A boolean flag to enable/disable HTTP/2."
  type        = bool
  default     = true
}
variable "alb_ip_address_type" {
  description = "The type of IP addresses used by the subnets for your load balancer."
  # The possible values are:
  #   * ipv4
  #   * dualstack
  type    = string
  default = "ipv4"
}


# ------------------------------------
# Default ALB Target Group parameters
# ------------------------------------
variable "alb_default_target_group_port" {
  description = "The port for the default target group."
  type        = number
  default     = 80
}
variable "alb_default_target_group_deregistration_delay" {
  description = "The amount of time to wait in seconds before changing the state of a deregistering target to unused."
  type        = number
  default     = 15
}
variable "alb_default_target_group_target_type" {
  description = "The type (`instance`, `ip` or `lambda`) of targets that can be registered with the target group."
  type        = string
  default     = "ip"
}

### Health-check settings ###
variable "alb_default_target_group_health_check_enabled" {
  description = "Indicates whether health checks are enabled."
  type        = bool
  default     = true
}
variable "alb_default_target_group_health_check_interval" {
  description = "The duration in seconds in between health checks."
  # Minimum value 5 seconds, maximum value 300 seconds. For `lambda` target groups, it needs to be greater as the
  # `timeout` of the underlying `lambda`.
  type    = number
  default = 15
}
variable "alb_default_target_group_health_check_path" {
  description = "The destination for the health check request."
  type        = string
  default     = "/"
}
variable "alb_default_target_group_health_check_timeout" {
  description = "The amount of time to wait in seconds before failing a health check request."
  type        = number
  default     = 10
}
variable "alb_default_target_group_health_check_healthy_threshold" {
  description = "The number of consecutive health checks successes required before considering an unhealthy target healthy."
  type        = number
  default     = 2
}
variable "alb_default_target_group_health_check_unhealthy_threshold" {
  description = "The number of consecutive health check failures required before considering the target unhealthy."
  type        = number
  default     = 2
}
variable "alb_default_target_group_health_check_matcher" {
  description = "The HTTP response codes to indicate a healthy check."
  type        = string
  default     = "200-399"
}


# -------------------------
# ALB HTTP/HTTPS listeners
# -------------------------
### HTTP Listener ###
variable "alb_http_listener_default_action_type" {
  description = "The type of routing action."
  # Valid values are:
  #   * forward
  #   * redirect
  #   * fixed-response
  #   * (not implemented) authenticate-cognito
  #   * (not implemented) authenticate-oidc.
  type    = string
  default = "forward"
}
### Parameters for `forward` action ###
variable "alb_http_listener_forward_target_group" {
  description = "The ARN of the Target Group to which to route traffic"
  type        = string
  default     = ""
}
### Parameters for `redirect` action ###
variable "alb_http_listener_redirect_host" {
  description = "The hostname. This component is not percent-encoded. The hostname can contain #{host}."
  type        = string
  default     = "#{host}"
}
variable "alb_http_listener_redirect_path" {
  description = "The absolute path, starting with the leading '/'. This component is not percent-encoded."
  # The path can contain #{host}, #{path}, and #{port}.
  type    = string
  default = "/#{path}"
}
variable "alb_http_listener_redirect_port" {
  description = "The port."
  # Specify a value from 1 to 65535 or #{port}.
  type    = string
  default = "#{port}"
}
variable "alb_http_listener_redirect_protocol" {
  description = "The protocol."
  # Valid values are:
  #   * HTTP
  #   * HTTPS
  #   #{protocol}.
  type    = string
  default = "#{protocol}"
}
variable "alb_http_listener_redirect_query" {
  description = "The query parameters, URL-encoded when necessary, but not percent-encoded."
  # Do not include the leading "?".
  type    = string
  default = "#{query}"
}
variable "alb_http_listener_redirect_status_code" {
  description = "The HTTP redirect code. The redirect is either permanent (HTTP_301) or temporary (HTTP_302)."
  type        = string
  default     = "HTTP_302"
}
### Parameters for `fixed-response` action ###
variable "alb_http_listener_fixed_response_content_type" {
  description = "The content type. Valid values are text/plain, text/css, text/html, application/javascript and application/json."
  type        = string
  default     = "text/plain"
}
variable "alb_http_listener_fixed_response_message_body" {
  description = "The message body."
  type        = string
  default     = ""
}
variable "alb_http_listener_fixed_response_status_code" {
  description = "The HTTP response code. Valid values are 2XX, 4XX, or 5XX."
  type        = string
  default     = "503"
}
######

### HTTPS Listener ###
variable "alb_https_listener_ssl_policy" {
  description = "The name of the SSL Policy for the listener."
  type        = string
  default     = "ELBSecurityPolicy-TLS-1-2-2017-01"
}
variable "alb_https_listener_certificate_arn" {
  description = "The ARN of the default SSL certificate for HTTPS listener."
  type        = string
  default     = ""
}
variable "alb_https_listener_default_action_type" {
  description = "The type of routing action."
  # Valid values are:
  #   * forward
  #   * redirect
  #   * fixed-response
  #   * (not implemented) authenticate-cognito
  #   * (not implemented) authenticate-oidc.
  type    = string
  default = "forward"
}
### Parameters for `forward` action ###
variable "alb_https_listener_forward_target_group" {
  description = "The ARN of the Target Group to which to route traffic"
  type        = string
  default     = ""
}
### Parameters for `redirect` action ###
variable "alb_https_listener_redirect_host" {
  description = "The hostname. This component is not percent-encoded. The hostname can contain #{host}."
  type        = string
  default     = "#{host}"
}
variable "alb_https_listener_redirect_path" {
  description = "The absolute path, starting with the leading '/'. This component is not percent-encoded."
  # The path can contain #{host}, #{path}, and #{port}.
  type    = string
  default = "/#{path}"
}
variable "alb_https_listener_redirect_port" {
  description = "The port."
  # Specify a value from 1 to 65535 or #{port}.
  type    = string
  default = "#{port}"
}
variable "alb_https_listener_redirect_protocol" {
  description = "The protocol."
  # Valid values are:
  #   * HTTP
  #   * HTTPS
  #   #{protocol}.
  type    = string
  default = "#{protocol}"
}
variable "alb_https_listener_redirect_query" {
  description = "The query parameters, URL-encoded when necessary, but not percent-encoded."
  #  Do not include the leading "?".
  type    = string
  default = "#{query}"
}
variable "alb_https_listener_redirect_status_code" {
  description = "The HTTP redirect code. The redirect is either permanent (HTTP_301) or temporary (HTTP_302)."
  type        = string
  default     = "HTTP_302"
}
### Parameters for `fixed_response` action ###
variable "alb_https_listener_fixed_response_content_type" {
  description = "The content type. Valid values are text/plain, text/css, text/html, application/javascript and application/json."
  type        = string
  default     = "text/plain"
}
variable "alb_https_listener_fixed_response_message_body" {
  description = "The message body."
  type        = string
  default     = ""
}
variable "alb_https_listener_fixed_response_status_code" {
  description = "The HTTP response code. Valid values are 2XX, 4XX, or 5XX."
  type        = string
  default     = "503"
}
######
