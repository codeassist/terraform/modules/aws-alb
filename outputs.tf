output "alb_id" {
  description = "The ARN of the load balancer (matches arn)."
  value       = join("", aws_lb.alb.*.id)
}

output "alb_arn" {
  description = "The ARN of the load balancer (matches id)."
  value       = join("", aws_lb.alb.*.arn)
}

output "alb_arn_suffix" {
  description = "The ARN suffix of the ALB for use with CloudWatch Metrics."
  value       = join("", aws_lb.alb.*.arn_suffix)
}

output "alb_dns_name" {
  description = "The DNS name of the load balancer."
  value       = join("", aws_lb.alb.*.dns_name)
}

output "alb_zone_id" {
  description = "The canonical hosted zone ID of the ALB provisioned (to be used in a Route 53 Alias record)."
  value       = join("", aws_lb.alb.*.zone_id)
}

output "alb_security_group_id" {
  description = "The security group ID of the ALB."
  value       = join("", aws_security_group.alb.*.id)
}

output "alb_default_target_group_arn" {
  description = "The default target group ARN."
  value       = join("", aws_lb_target_group.default.*.arn)
}

output "alb_http_listener_arn" {
  description = "The ARN of the HTTP listener."
  value       = join("", aws_lb_listener.http.*.arn)
}

output "alb_https_listener_arn" {
  description = "The ARN of the HTTPS listener."
  value       = join("", aws_lb_listener.https.*.arn)
}

output "alb_access_logs_bucket_id" {
  description = "The S3 bucket ID for access logs"
  value       = (local.enabled && var.alb_access_logs_enabled) ? module.access_logs.s3_lb_logs_bucket_id : ""
}
