# --------------------------
# General/Common parameters
# --------------------------
# Whether to create the resources ("false" prevents the module from creating any resources).
alb_module_enabled = false
# (Required) The name of the LB.
alb_name = "test-1"
# A mapping of tags to assign to the resource.
alb_tags = {}
# ----------------------------
# Network/Security parameters
# ----------------------------
# (Required) VPC ID to associate with ALB.
alb_vpc_id = "vpc-test1"
# A boolean flag to enable/disable HTTP listener.
alb_http_listener_enabled = true
# The port for the HTTP listener.
alb_http_listener_port = 80
# List of CIDR blocks to allow in HTTP security group.
alb_http_ingress_cidr_blocks = ["0.0.0.0/0"]
# List of prefix list IDs for allowing access to HTTP ingress security group.
alb_http_ingress_prefix_list_ids = []
# A boolean flag to enable/disable HTTPS listener.
alb_https_listener_enabled = false
# The port for the HTTPS listener.
alb_https_listener_port = 443
# List of CIDR blocks to allow in HTTPS security group.
alb_https_ingress_cidr_blocks = ["0.0.0.0/0"]
# List of prefix list IDs for allowing access to HTTPS ingress security group.
alb_https_ingress_prefix_list_ids = []
# --------------------------
# S3 Logs bucket parameters
# --------------------------
# A boolean flag to enable/disable access_logs.
alb_access_logs_enabled = false
# A boolean that indicates all objects should be deleted from the ALB access logs S3 bucket so that the bucket
# can be destroyed without error.
alb_access_logs_force_destroy_s3_bucket = false
# ---------------
# ALB parameters
# ---------------
# A boolean flag to determine whether the ALB should be internal.
alb_internal = false
# A list of additional security group IDs to assign to the ALB.
alb_security_group_ids = []
# (Required) A list of subnet IDs to associate with ALB.
alb_subnet_ids = [
  "subnet-test-1-1",
  "subnet-test-1-2",
]
# The time in seconds that the connection is allowed to be idle.
alb_idle_timeout = 60
# A boolean flag to enable/disable deletion protection for ALB.
alb_deletion_protection_enabled = false
# A boolean flag to enable/disable cross zone load balancing.
alb_cross_zone_load_balancing_enabled = true
# A boolean flag to enable/disable HTTP/2.
alb_http2_enabled = true
# The type of IP addresses used by the subnets for your load balancer.
alb_ip_address_type = "ipv4"
# ------------------------------------
# Default ALB Target Group parameters
# ------------------------------------
# The port for the default target group.
alb_default_target_group_port = 80
# The amount of time to wait in seconds before changing the state of a deregistering target to unused.
alb_default_target_group_deregistration_delay = 15
# The type ("instance", "ip" or "lambda") of targets that can be registered with the target group.
alb_default_target_group_target_type = "ip"
### Health-check settings ###
# Indicates whether health checks are enabled.
alb_default_target_group_health_check_enabled = true
# The duration in seconds in between health checks.
alb_default_target_group_health_check_interval = 15
# The destination for the health check request.
alb_default_target_group_health_check_path = "/"
# The amount of time to wait in seconds before failing a health check request.
alb_default_target_group_health_check_timeout = 10
# The number of consecutive health checks successes required before considering an unhealthy target healthy.
alb_default_target_group_health_check_healthy_threshold = 2
# The number of consecutive health check failures required before considering the target unhealthy.
alb_default_target_group_health_check_unhealthy_threshold = 2
# The HTTP response codes to indicate a healthy check.
alb_default_target_group_health_check_matcher = "200-399"
# -------------------------
# ALB HTTP/HTTPS listeners
# -------------------------
### HTTP Listener ###
# The type of routing action.
alb_http_listener_default_action_type = "redirect"
### Parameters for `redirect` action ###
# The hostname. This component is not percent-encoded. The hostname can contain #{host}.
alb_http_listener_redirect_host = "#{host}"
# The absolute path, starting with the leading '/'. This component is not percent-encoded.
alb_http_listener_redirect_path = "/#{path}"
# The port.
alb_http_listener_redirect_port = "#{port}"
# The protocol.
alb_http_listener_redirect_protocol = "#{protocol}"
# The query parameters, URL-encoded when necessary, but not percent-encoded.
alb_http_listener_redirect_query = "#{query}"
# The HTTP redirect code. The redirect is either permanent (HTTP_301) or temporary (HTTP_302).
alb_http_listener_redirect_status_code = "HTTP_302"
### Parameters for `fixed-response` action ###
# The content type. Valid values are text/plain, text/css, text/html, application/javascript and application/json.
alb_http_listener_fixed_response_content_type = "text/plain"
# The message body.
alb_http_listener_fixed_response_message_body = ""
# The HTTP response code. Valid values are 2XX, 4XX, or 5XX.
alb_http_listener_fixed_response_status_code = "503"
### HTTPS Listener ###
# The name of the SSL Policy for the listener.
alb_https_listener_ssl_policy = "ELBSecurityPolicy-TLS-1-2-2017-01"
# The ARN of the default SSL certificate for HTTPS listener.
alb_https_listener_certificate_arn = ""
# The type of routing action.
alb_https_listener_default_action_type = "forward"
### Parameters for `redirect` action ###
# The hostname. This component is not percent-encoded. The hostname can contain #{host}.
alb_https_listener_redirect_host = "#{host}"
# The absolute path, starting with the leading '/'. This component is not percent-encoded.
alb_https_listener_redirect_path = "/#{path}"
# The port.
alb_https_listener_redirect_port = "#{port}"
# The protocol.
alb_https_listener_redirect_protocol = "#{protocol}"
# The query parameters, URL-encoded when necessary, but not percent-encoded.
alb_https_listener_redirect_query = "#{query}"
# The HTTP redirect code. The redirect is either permanent (HTTP_301) or temporary (HTTP_302).
alb_https_listener_redirect_status_code = "HTTP_302"
### Parameters for `fixed_response` action ###
# The content type. Valid values are text/plain, text/css, text/html, application/javascript and application/json.
alb_https_listener_fixed_response_content_type = "text/plain"
# The message body.
alb_https_listener_fixed_response_message_body = ""
# The HTTP response code. Valid values are 2XX, 4XX, or 5XX.
alb_https_listener_fixed_response_status_code = "503"
